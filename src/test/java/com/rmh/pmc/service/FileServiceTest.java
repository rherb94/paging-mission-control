package com.rmh.pmc.service;

import com.rmh.pmc.domain.TelemetryData;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
class FileServiceTest {
	@InjectMocks
	private FileService fileService;

	//@Test
//	void shouldThrowFilesNotFoundException() {
//		Exception exception = assertThrows(FileNotFoundException.class, () -> {
//			Map<Long, List<TelemetryData>> dataMap = fileService.loadInputFile("newFile.txt");
//		});
//
//		String expectedMessage = "doesNotExist.txt not found";
//		String actualMessage = exception.getMessage();
//
//		assertTrue(true, actualMessage.contains(expectedMessage));
//	}

	@Test
	void shouldCreateMapWithTelemetryData() throws Exception {
		Map<Long, List<TelemetryData>> tdMap = fileService.loadInputFile("sampleData.txt");
		int totalCount = 0;
		for (List<TelemetryData> list : tdMap.values()) {
			totalCount += list.size();
		}
		assertEquals(16, totalCount);
		assertEquals(2, tdMap.size());
	}
}