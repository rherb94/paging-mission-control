package com.rmh.pmc.service;

import com.rmh.pmc.domain.Alarm;
import com.rmh.pmc.domain.Component;
import com.rmh.pmc.domain.Severity;
import com.rmh.pmc.domain.TelemetryData;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
class AlarmServiceTest {

	@InjectMocks
	private AlarmService alarmService;


	//Three RED HIGH on TSTAT in a 5-minute interval should produce 1 alarm
	@Test
	void shouldProduceSingleTstatAlarm() {
		Map<Long, List<TelemetryData>> tdMap = buildTstatAlarm();
		List<Alarm> alarms = alarmService.getAlarms(tdMap);
		assertEquals(1, alarms.size());
		assertEquals(Severity.RED_HIGH, alarms.getFirst().getSeverity());
		assertEquals(Component.TSTAT, alarms.getFirst().getComponent());
	}

	//Three RED LOW on BATT in a 5-minute interval should produce 1 alarm
	@Test
	void shouldProduceSingleBattAlarm() {
		Map<Long, List<TelemetryData>> tdMap = buildBattAlarm();
		List<Alarm> alarms = alarmService.getAlarms(tdMap);
		assertEquals(1, alarms.size());
		assertEquals(Severity.RED_LOW, alarms.getFirst().getSeverity());
		assertEquals(Component.BATT, alarms.getFirst().getComponent());
	}

	@Test
	void shouldProduceOneAlarmEach() {
		List<TelemetryData> tdList = new ArrayList<>();
		tdList.addAll(buildTstatAlarm().get(1000L));
		tdList.addAll(buildBattAlarm().get(1000L));
		Map<Long, List<TelemetryData>> tdMap = new HashMap<>();
		tdMap.put(1000L, tdList);
		List<Alarm> alarms = alarmService.getAlarms(tdMap);
		assertEquals(2, alarms.size());
		long tstatAlarms = alarms.stream()
				.filter(alarm -> alarm.getSeverity() == Severity.RED_HIGH && alarm.getComponent() == Component.TSTAT)
				.count();
		assertEquals(tstatAlarms, 1L);

		long battAlarms = alarms.stream()
				.filter(alarm -> alarm.getSeverity() == Severity.RED_LOW && alarm.getComponent() == Component.BATT)
				.count();
		assertEquals(battAlarms, 1L);
	}

	//Should produce 1 alarm for each sat id on TSTAT
	@Test
	void shouldProduceOneTstatAlarmForEachSatelliteId() {
		Map<Long, List<TelemetryData>> tdMap = buildTstatAlarmMultipleSatelliteIds();
		List<Alarm> alarms = alarmService.getAlarms(tdMap);
		assertEquals(2, alarms.size());
		long tstatAlarms = alarms.stream()
				.filter(alarm -> alarm.getSeverity() == Severity.RED_HIGH && alarm.getComponent() == Component.TSTAT)
				.count();
		assertEquals(tstatAlarms, 2L);
	}

	//Should not produce an alarm when only 2 alarm cases found inside 5 min interval
	@Test
	void shouldNotProduceAlarmForOnlyTwoAlarmCaseInTimeInterval() {
		Map<Long, List<TelemetryData>> tdMap = buildThreeAlarmOutsideTimeInterval();
		List<Alarm> alarms = alarmService.getAlarms(tdMap);
		assertEquals(0, alarms.size());
	}

	//Test adding different alarm cases over 60 minute interval
	@Test
	void shouldProduceProperAlarmCount() {
		Map<Long, List<TelemetryData>> tdMap = buildAlarmDataOverAnHour();
		List<Alarm> alarms = alarmService.getAlarms(tdMap);
		assertEquals(2, alarms.size());
		long tstatAlarms = alarms.stream()
				.filter(alarm -> alarm.getSeverity() == Severity.RED_HIGH && alarm.getComponent() == Component.TSTAT)
				.count();
		assertEquals(tstatAlarms, 1L);

		long battAlarms = alarms.stream()
				.filter(alarm -> alarm.getSeverity() == Severity.RED_LOW && alarm.getComponent() == Component.BATT)
				.count();
		assertEquals(battAlarms, 1L);

	}

	//Test adding different alarm cases over 60 minute interval with different sat ids
	@Test
	void shouldProduceOneAlarmPerSatIdAndComponent() {
		Map<Long, List<TelemetryData>> tdMap = buildComplexTelemData();
		List<Alarm> alarms = alarmService.getAlarms(tdMap);
		assertEquals(4, alarms.size());
		long tstatAlarms = alarms.stream()
				.filter(alarm -> alarm.getSeverity() == Severity.RED_HIGH && alarm.getComponent() == Component.TSTAT)
				.count();
		assertEquals(tstatAlarms, 1L);

		//total batt alarms should be 3
		long battAlarms = alarms.stream()
				.filter(alarm -> alarm.getSeverity() == Severity.RED_LOW && alarm.getComponent() == Component.BATT)
				.count();
		assertEquals(battAlarms, 3L);
		//batt alarms for sat 1000 should be 2
		long battAlarmsSat1 = alarms.stream()
				.filter(alarm -> alarm.getSeverity() == Severity.RED_LOW && alarm.getComponent() == Component.BATT && alarm.getSatelliteId().equals(1000L))
				.count();
		assertEquals(battAlarmsSat1, 2L);
		//batt alarms for sat 1001 should be 1
		long battAlarmsSat2 = alarms.stream()
				.filter(alarm -> alarm.getSeverity() == Severity.RED_LOW && alarm.getComponent() == Component.BATT && alarm.getSatelliteId().equals(1001L))
				.count();
		assertEquals(battAlarmsSat2, 1L);

	}


	private Map<Long, List<TelemetryData>> buildTstatAlarm() {
		Map<Long, List<TelemetryData>> tdMap = new HashMap<>();
		Instant currentTimeStamp = Instant.now();

		List<TelemetryData> tdList = new ArrayList<>();
		TelemetryData td1 =
				TelemetryData.builder().timeStamp(currentTimeStamp).satelliteId(1000L).redHighLimit(101D).yellowHighLimit(98D).yellowLowLimit(25D).redLowLimit(20D).rawValue(109.9).component(Component.TSTAT).build();
		TelemetryData td2 =
				TelemetryData.builder().timeStamp(currentTimeStamp.plus(Duration.ofMinutes(1))).satelliteId(1000L).redHighLimit(101D).yellowHighLimit(98D).yellowLowLimit(25D).redLowLimit(20D).rawValue(109.9).component(Component.TSTAT).build();
		TelemetryData td3 =
				TelemetryData.builder().timeStamp(currentTimeStamp.plus(Duration.ofMinutes(2))).satelliteId(1000L).redHighLimit(101D).yellowHighLimit(98D).yellowLowLimit(25D).redLowLimit(20D).rawValue(109.9).component(Component.TSTAT).build();
		tdList.add(td1);
		tdList.add(td2);
		tdList.add(td3);

		tdMap.put(1000L, tdList);
		return tdMap;
	}

	private Map<Long, List<TelemetryData>> buildTstatAlarmMultipleSatelliteIds() {
		Map<Long, List<TelemetryData>> tdMap = new HashMap<>();
		Instant currentTimeStamp = Instant.now();
		//1001 sat ids
		List<TelemetryData> tdList = new ArrayList<>();
		TelemetryData td1 =
				TelemetryData.builder().timeStamp(currentTimeStamp).satelliteId(1000L).redHighLimit(101D).yellowHighLimit(98D).yellowLowLimit(25D).redLowLimit(20D).rawValue(109.9D).component(Component.TSTAT).build();
		TelemetryData td2 =
				TelemetryData.builder().timeStamp(currentTimeStamp.plus(Duration.ofMinutes(1))).satelliteId(1000L).redHighLimit(101D).yellowHighLimit(98D).yellowLowLimit(25D).redLowLimit(20D).rawValue(109.9D).component(Component.TSTAT).build();
		TelemetryData td3 =
				TelemetryData.builder().timeStamp(currentTimeStamp.plus(Duration.ofMinutes(2))).satelliteId(1000L).redHighLimit(101D).yellowHighLimit(98D).yellowLowLimit(25D).redLowLimit(20D).rawValue(109.9D).component(Component.TSTAT).build();
		tdList.add(td1);
		tdList.add(td2);
		tdList.add(td3);
		tdMap.put(1001L, tdList);

		//1000 sat ids
		List<TelemetryData> tdList2 = new ArrayList<>();
		TelemetryData td4 =
				TelemetryData.builder().timeStamp(currentTimeStamp).satelliteId(1000L).redHighLimit(101D).yellowHighLimit(98D).yellowLowLimit(25D).redLowLimit(20D).rawValue(109.9D).component(Component.TSTAT).build();
		TelemetryData td5 =
				TelemetryData.builder().timeStamp(currentTimeStamp.plus(Duration.ofMinutes(1))).satelliteId(1000L).redHighLimit(101D).yellowHighLimit(98D).yellowLowLimit(25D).redLowLimit(20D).rawValue(109.9D).component(Component.TSTAT).build();
		TelemetryData td6 =
				TelemetryData.builder().timeStamp(currentTimeStamp.plus(Duration.ofMinutes(2))).satelliteId(1000L).redHighLimit(101D).yellowHighLimit(98D).yellowLowLimit(25D).redLowLimit(20D).rawValue(109.9D).component(Component.TSTAT).build();
		tdList2.add(td4);
		tdList2.add(td5);
		tdList2.add(td6);
		tdMap.put(1000L, tdList2);

		return tdMap;
	}

	private Map<Long, List<TelemetryData>> buildThreeAlarmOutsideTimeInterval() {
		Map<Long, List<TelemetryData>> tdMap = new HashMap<>();
		Instant currentTimeStamp = Instant.now();
		//1001 sat ids
		List<TelemetryData> tdList = new ArrayList<>();
		TelemetryData td1 =
				//Count = 1
				TelemetryData.builder().timeStamp(currentTimeStamp).satelliteId(1001L).redHighLimit(101D).yellowHighLimit(98D).yellowLowLimit(25D).redLowLimit(20D).rawValue(109.9D).component(Component.TSTAT).build();
		TelemetryData td2 =
				//Count = 2
				TelemetryData.builder().timeStamp(currentTimeStamp.plus(Duration.ofMinutes(1))).satelliteId(1001L).redHighLimit(101D).yellowHighLimit(98D).yellowLowLimit(25D).redLowLimit(20D).rawValue(109.9D).component(Component.TSTAT).build();
		TelemetryData td3 =
				TelemetryData.builder().timeStamp(currentTimeStamp.plus(Duration.ofMinutes(10))).satelliteId(1001L).redHighLimit(101D).yellowHighLimit(98D).yellowLowLimit(25D).redLowLimit(20D).rawValue(109.9D).component(Component.TSTAT).build();
		tdList.add(td1);
		tdList.add(td2);
		tdList.add(td3);
		tdMap.put(1001L, tdList);


		return tdMap;
	}

	private Map<Long, List<TelemetryData>> buildAlarmDataOverAnHour() {
		Map<Long, List<TelemetryData>> tdMap = new HashMap<>();
		Instant currentTimeStamp = Instant.now();
		//TSTAT ALARMS - 1001 sat ids
		List<TelemetryData> tdList = new ArrayList<>();
		TelemetryData tstat1 =
				//TSTAT Alarm count 1
				TelemetryData.builder().timeStamp(currentTimeStamp).satelliteId(1001L).redHighLimit(101D).yellowHighLimit(98D).yellowLowLimit(25D).redLowLimit(20D).rawValue(109.9D).component(Component.TSTAT).build();
		TelemetryData tstat2 =
				//Count = 2
				TelemetryData.builder().timeStamp(currentTimeStamp.plus(Duration.ofMinutes(1L))).satelliteId(1001L).redHighLimit(101D).yellowHighLimit(98D).yellowLowLimit(25D).redLowLimit(20D).rawValue(102D).component(Component.TSTAT).build();
		TelemetryData tstat3 =
				TelemetryData.builder().timeStamp(currentTimeStamp.plus(Duration.ofMinutes(4L))).satelliteId(1001L).redHighLimit(101D).yellowHighLimit(98D).yellowLowLimit(25D).redLowLimit(20D).rawValue(101.9D).component(Component.TSTAT).build();
		tdList.add(tstat1);
		tdList.add(tstat2);
		tdList.add(tstat3);

		TelemetryData batt1 =
				//BATT ALARM COUNT 1
				TelemetryData.builder().timeStamp(currentTimeStamp).satelliteId(1001L).redHighLimit(17D).yellowHighLimit(15D).yellowLowLimit(9D).redLowLimit(20D).rawValue(19.9D).component(Component.BATT).build();
		TelemetryData batt2 =
				//BATT ALARM COUNT 2
				TelemetryData.builder().timeStamp(currentTimeStamp.plus(Duration.ofMinutes(3L))).satelliteId(1001L).redHighLimit(17D).yellowHighLimit(15D).yellowLowLimit(9D).redLowLimit(20D).rawValue(18D).component(Component.BATT).build();
		TelemetryData batt3 =
				//BATT ALARM COUNT 3
				TelemetryData.builder().timeStamp(currentTimeStamp.plus(Duration.ofMinutes(4L)).plus(Duration.ofSeconds(30L))).satelliteId(1001L).redHighLimit(17D).yellowHighLimit(15D).yellowLowLimit(9D).redLowLimit(20D).rawValue(17D).component(Component.BATT).build();
		TelemetryData batt4 =
				//BATT ALARM - Outside time window
				TelemetryData.builder().timeStamp(currentTimeStamp.plus(Duration.ofMinutes(30L)).plus(Duration.ofSeconds(30L))).satelliteId(1001L).redHighLimit(17D).yellowHighLimit(15D).yellowLowLimit(9D).redLowLimit(20D).rawValue(17D).component(Component.BATT).build();
		TelemetryData batt5 =
				//BATT ALARM COUNT - Outside time window
				TelemetryData.builder().timeStamp(currentTimeStamp.plus(Duration.ofHours(24L)).plus(Duration.ofSeconds(30L))).satelliteId(1001L).redHighLimit(17D).yellowHighLimit(15D).yellowLowLimit(9D).redLowLimit(20D).rawValue(17D).component(Component.BATT).build();

		tdList.add(batt1);
		tdList.add(batt2);
		tdList.add(batt3);
		tdList.add(batt4);
		tdList.add(batt5);

		tdMap.put(1001L, tdList);


		return tdMap;
	}

	private Map<Long, List<TelemetryData>> buildOtherSatelliteData() {
		Map<Long, List<TelemetryData>> tdMap = new HashMap<>();
		Instant currentTimeStamp = Instant.now();
		//TSTAT ALARMS - 1000 sat ids
		List<TelemetryData> tdList = new ArrayList<>();
		TelemetryData tstat1 =
				//TSTAT Alarm count 1
				TelemetryData.builder().timeStamp(currentTimeStamp).satelliteId(1000L).redHighLimit(101D).yellowHighLimit(98D).yellowLowLimit(25D).redLowLimit(20D).rawValue(109.9D).component(Component.TSTAT).build();
		TelemetryData tstat2 =
				//TSTAT Count = 2
				TelemetryData.builder().timeStamp(currentTimeStamp.plus(Duration.ofMinutes(1L))).satelliteId(1000L).redHighLimit(101D).yellowHighLimit(98D).yellowLowLimit(25D).redLowLimit(20D).rawValue(102D).component(Component.TSTAT).build();
		//TSTAT reading outside time window
		TelemetryData tstat3 =
				TelemetryData.builder().timeStamp(currentTimeStamp.plus(Duration.ofMinutes(45L))).satelliteId(1000L).redHighLimit(101D).yellowHighLimit(98D).yellowLowLimit(25D).redLowLimit(20D).rawValue(101.9D).component(Component.TSTAT).build();
		tdList.add(tstat1);
		tdList.add(tstat2);
		tdList.add(tstat3);

		TelemetryData batt1 =
				//BATT ALARM COUNT 1
				TelemetryData.builder().timeStamp(currentTimeStamp).satelliteId(1000L).redHighLimit(17D).yellowHighLimit(15D).yellowLowLimit(9D).redLowLimit(20D).rawValue(19.9D).component(Component.BATT).build();
		TelemetryData batt2 =
				//BATT ALARM COUNT 2
				TelemetryData.builder().timeStamp(currentTimeStamp.plus(Duration.ofMinutes(3L))).satelliteId(1000L).redHighLimit(17D).yellowHighLimit(15D).yellowLowLimit(9D).redLowLimit(20D).rawValue(18D).component(Component.BATT).build();
		TelemetryData batt3 =
				//BATT ALARM COUNT 3
				TelemetryData.builder().timeStamp(currentTimeStamp.plus(Duration.ofMinutes(4L)).plus(Duration.ofSeconds(30L))).satelliteId(1000L).redHighLimit(17D).yellowHighLimit(15D).yellowLowLimit(9D).redLowLimit(20D).rawValue(15D).component(Component.BATT).build();
		TelemetryData batt4 =
				//BATT ALARM - Outside time window
				TelemetryData.builder().timeStamp(currentTimeStamp.plus(Duration.ofMinutes(35L)).plus(Duration.ofSeconds(30L))).satelliteId(1000L).redHighLimit(17D).yellowHighLimit(15D).yellowLowLimit(9D).redLowLimit(20D).rawValue(17D).component(Component.BATT).build();
		TelemetryData batt5 =
				//BATT ALARM COUNT - Outside time window
				TelemetryData.builder().timeStamp(currentTimeStamp.plus(Duration.ofHours(24L)).plus(Duration.ofSeconds(30L))).satelliteId(1000L).redHighLimit(17D).yellowHighLimit(15D).yellowLowLimit(9D).redLowLimit(20D).rawValue(17D).component(Component.BATT).build();

		tdList.add(batt1);
		tdList.add(batt2);
		tdList.add(batt3);
		tdList.add(batt4);
		tdList.add(batt5);
		//test adding new alarms in different time window
		TelemetryData batt6 =
				//BATT ALARM COUNT 1
				TelemetryData.builder().timeStamp(currentTimeStamp.plus(Duration.ofMinutes(5L).plus(Duration.ofSeconds(5L)))).satelliteId(1000L).redHighLimit(17D).yellowHighLimit(15D).yellowLowLimit(9D).redLowLimit(20D).rawValue(19.9D).component(Component.BATT).build();
		TelemetryData batt7 =
				//BATT ALARM COUNT 2
				TelemetryData.builder().timeStamp(currentTimeStamp.plus(Duration.ofMinutes(6L))).satelliteId(1000L).redHighLimit(17D).yellowHighLimit(15D).yellowLowLimit(9D).redLowLimit(20D).rawValue(18D).component(Component.BATT).build();
		TelemetryData batt8 =
				//BATT ALARM COUNT 3
				TelemetryData.builder().timeStamp(currentTimeStamp.plus(Duration.ofMinutes(7L)).plus(Duration.ofSeconds(30L))).satelliteId(1000L).redHighLimit(17D).yellowHighLimit(15D).yellowLowLimit(9D).redLowLimit(20D).rawValue(15D).component(Component.BATT).build();
		TelemetryData batt9 =
				//BATT ALARM COUNT 4
				TelemetryData.builder().timeStamp(currentTimeStamp.plus(Duration.ofMinutes(8L)).plus(Duration.ofSeconds(30L))).satelliteId(1000L).redHighLimit(17D).yellowHighLimit(15D).yellowLowLimit(9D).redLowLimit(20D).rawValue(17D).component(Component.BATT).build();
		TelemetryData batt10 =
				//BATT ALARM COUNT 5
				TelemetryData.builder().timeStamp(currentTimeStamp.plus(Duration.ofHours(9L)).plus(Duration.ofSeconds(30L))).satelliteId(1000L).redHighLimit(17D).yellowHighLimit(15D).yellowLowLimit(9D).redLowLimit(20D).rawValue(17D).component(Component.BATT).build();

		tdList.add(batt6);
		tdList.add(batt7);
		tdList.add(batt8);
		tdList.add(batt9);
		tdList.add(batt10);


		tdMap.put(1000L, tdList);


		return tdMap;
	}

	private Map<Long, List<TelemetryData>> buildComplexTelemData() {
		Map<Long, List<TelemetryData>> tdMap = buildAlarmDataOverAnHour();
		tdMap.put(1000L, buildOtherSatelliteData().get(1000L));
		return tdMap;
	}


	private Map<Long, List<TelemetryData>> buildBattAlarm() {
		Map<Long, List<TelemetryData>> tdMap = new HashMap<>();
		Instant currentTimeStamp = Instant.now();
		List<TelemetryData> tdList = new ArrayList<>();
		TelemetryData td1 =
				TelemetryData.builder().timeStamp(currentTimeStamp).satelliteId(1000L).redHighLimit(17D).yellowHighLimit(15D).yellowLowLimit(9D).redLowLimit(20D).rawValue(19.9D).component(Component.BATT).build();
		TelemetryData td2 =
				TelemetryData.builder().timeStamp(currentTimeStamp.plus(Duration.ofMinutes(1))).satelliteId(1000L).redHighLimit(17D).yellowHighLimit(15D).yellowLowLimit(9D).redLowLimit(20D).rawValue(18D).component(Component.BATT).build();
		TelemetryData td3 =
				TelemetryData.builder().timeStamp(currentTimeStamp.plus(Duration.ofMinutes(2))).satelliteId(1000L).redHighLimit(17D).yellowHighLimit(15D).yellowLowLimit(9D).redLowLimit(20D).rawValue(17D).component(Component.BATT).build();
		tdList.add(td1);
		tdList.add(td2);
		tdList.add(td3);

		tdMap.put(1000L, tdList);
		return tdMap;
	}

}