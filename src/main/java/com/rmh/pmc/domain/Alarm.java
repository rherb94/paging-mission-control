package com.rmh.pmc.domain;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Alarm {
	private Long satelliteId;
	private Severity severity;
	private Component component;
	private String timestamp;

}