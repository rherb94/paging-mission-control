package com.rmh.pmc.domain;

import lombok.Builder;
import lombok.Data;

import java.util.List;
import java.util.stream.Collectors;

@Data
@Builder
public class AlarmDto {
	private Long satelliteId;
	private String severity;
	private Component component;
	private String timestamp;

	public static List<AlarmDto> getAlarmDTO(List<Alarm> alarms) {
		return alarms.stream()
				.map(alarm -> AlarmDto.builder()
						.satelliteId(alarm.getSatelliteId())
						.severity(getSeverityDisplay(alarm.getSeverity()))
						.component(alarm.getComponent())
						.timestamp(alarm.getTimestamp())
						.build())
				.collect(Collectors.toList());
	}

	public static String getSeverityDisplay(Severity severity) {
		return switch (severity) {
			case RED_HIGH -> ("RED HIGH");
			case RED_LOW -> ("RED LOW");
			default -> null;
		};
	}

}

