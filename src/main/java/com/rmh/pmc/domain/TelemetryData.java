package com.rmh.pmc.domain;

import lombok.Builder;
import lombok.Data;

import java.time.Instant;

@Data
@Builder
public class TelemetryData {
	private Instant timeStamp;
	private Long satelliteId;
	private Double redHighLimit;
	private Double yellowHighLimit;
	private Double yellowLowLimit;
	private Double redLowLimit;
	private Double rawValue;
	private Component component;

	public Severity getAlarm() {
		if (rawValue < redLowLimit) {
			return Severity.RED_LOW;
		} else if (rawValue > redHighLimit) {
			return Severity.RED_HIGH;
		}
		return null;
	}
}
