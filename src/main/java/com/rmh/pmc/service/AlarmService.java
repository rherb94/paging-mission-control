package com.rmh.pmc.service;

import com.rmh.pmc.domain.Alarm;
import com.rmh.pmc.domain.Component;
import com.rmh.pmc.domain.Severity;
import com.rmh.pmc.domain.TelemetryData;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
@Slf4j
public class AlarmService {
	private final List<Alarm> alarms = new ArrayList<>();

	@SneakyThrows
	public List<Alarm> getAlarms(Map<Long, List<TelemetryData>> inputDataMap) {
		// loop through each entry in the map
		for (Map.Entry<Long, List<TelemetryData>> entry : inputDataMap.entrySet()) {
			// Loop through each 5-minute interval
			Instant intervalStart = entry.getValue().getFirst().getTimeStamp();
			Instant intervalEnd = intervalStart.plus(Duration.ofMinutes(5));
			List<TelemetryData> currentIntervalData = new ArrayList<>();
			for (TelemetryData data : entry.getValue()) {
				Instant timestamp = data.getTimeStamp();
				// Continue adding entries to currentIntervalData until we reach 5 minutes past the intervalStart reading...
				if (timestamp.isBefore(intervalEnd)) {
					currentIntervalData.add(data);
				} else {
					// Process the current interval data
					processIntervalData(currentIntervalData);
					// After processing, move to the next interval...
					intervalStart = intervalEnd;
					intervalEnd = intervalStart.plus(Duration.ofMinutes(5));
					currentIntervalData.clear();
				}
			}
			// Process the last interval data
			processIntervalData(currentIntervalData);
		}
		return alarms;
	}

	private void processIntervalData(List<TelemetryData> currentIntervalData) {

		// Filter the interval data for TSTAT alarm readings
		List<TelemetryData> tstatAlarmReading = currentIntervalData.stream()
				.filter(data -> data.getComponent() == Component.TSTAT && data.getAlarm() == Severity.RED_HIGH)
				.toList();

		if (tstatAlarmReading.size() >= 3) {
			alarms.add(createAlarm(tstatAlarmReading.getFirst()));
		}

		// Do the same for BATT...
		List<TelemetryData> battAlarmReading = currentIntervalData.stream()
				.filter(data -> data.getComponent() == Component.BATT && data.getAlarm() == Severity.RED_LOW).toList();

		if (battAlarmReading.size() >= 3) {
			alarms.add(createAlarm(battAlarmReading.getFirst()));
		}
	}

	private Alarm createAlarm(TelemetryData td) {
		return Alarm.builder()
				.satelliteId(td.getSatelliteId())
				.severity(td.getAlarm())
				.component(td.getComponent())
				.timestamp(td.getTimeStamp().toString())
				.build();
	}
}
