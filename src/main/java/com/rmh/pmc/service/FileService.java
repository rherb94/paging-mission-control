package com.rmh.pmc.service;

import com.rmh.pmc.domain.Component;
import com.rmh.pmc.domain.TelemetryData;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.FileNotFoundException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Service
@Slf4j
public class FileService {
	private final String timePattern = "yyyyMMdd HH:mm:ss.SSS";
	private final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(timePattern);

	public Map<Long, List<TelemetryData>> loadInputFile(String inputFile) throws Exception {
		Map<Long, List<TelemetryData>> telemetryDataMap = new HashMap<>();
		log.debug("loading data file: {}", inputFile);

		Path path = Paths.get(inputFile);
		if (!Files.exists(path)) {
			throw new FileNotFoundException(inputFile + " not found");
		}

		try {
			List<String> allLines = Files.readAllLines(path);
			int lineNumber = 0;
			for (String line : allLines) {
				lineNumber++;
				String[] data = line.split("\\|");
				if (data.length != 8) {
					log.error("Error parsing data on line: {}", lineNumber);
					continue;
				}
				LocalDateTime readingTime = LocalDateTime.parse(data[0], dateTimeFormatter);

				TelemetryData td = TelemetryData.builder()
						.timeStamp(readingTime.toInstant(ZoneOffset.UTC))
						.satelliteId(Long.valueOf(data[1]))
						.redHighLimit(Double.valueOf(data[2]))
						.yellowHighLimit(Double.valueOf(data[3]))
						.yellowLowLimit(Double.valueOf(data[4]))
						.redLowLimit(Double.valueOf(data[5]))
						.rawValue(Double.valueOf(data[6]))
						.component(getComponentType(data[7]))
						.build();
				if (telemetryDataMap.containsKey(td.getSatelliteId())) {
					telemetryDataMap.get(td.getSatelliteId()).add(td);
				} else {
					telemetryDataMap.put(td.getSatelliteId(), new ArrayList<>());
					telemetryDataMap.get(td.getSatelliteId()).add(td);

				}

			}
		} catch (Exception e) {
			log.error("Exception occurred while reading data file, error: {}", e.getMessage());
		}

		for (Map.Entry<Long, List<TelemetryData>> entry : telemetryDataMap.entrySet()) {
			List<TelemetryData> telemetryList = entry.getValue();
			telemetryList.sort(Comparator.comparing(TelemetryData::getTimeStamp));
		}

		return telemetryDataMap;
	}

	private Component getComponentType(String componentName) {
		if (componentName.equals("TSTAT")) {
			return Component.TSTAT;
		} else if (componentName.equals("BATT")) {
			return Component.BATT;
		} else {
			throw new RuntimeException("invalid component name");
		}
	}
}
