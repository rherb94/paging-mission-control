package com.rmh.pmc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rmh.pmc.domain.AlarmDto;
import com.rmh.pmc.service.AlarmService;
import com.rmh.pmc.service.FileService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.List;

@SpringBootApplication
@Slf4j
@AllArgsConstructor
public class PagingMissionControl implements CommandLineRunner {
	private final AlarmService alarmService;
	private final FileService fileService;
	private final ObjectMapper objectMapper;

	public static void main(String[] args) {
		SpringApplication.run(PagingMissionControl.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		 if (args.length == 0) {
			 log.error("Error, no file name passed in.");
			 System.exit(0);
		 }

		List<AlarmDto> alarms = AlarmDto.getAlarmDTO(alarmService.getAlarms(fileService.loadInputFile(args[0])));
		log.info("alarms: \n{}", objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(alarms));
	}
}
